package resource.fxml;

import javafx.collections.ObservableList;

/**
 * Static Class to load {@link java.lang.Enum Enums} as
 * {@link javafx.collections.ObservableList ObservableList} into an FXML-File.
 * @author Mario Schäper
 */
public abstract class EnumLoader {
	/**
	 * Returns the {@link java.lang.Class Class} of the Elements of an
	 * {@link javafx.collections.ObservableList ObservableList}.<br/>
	 * This Method is necessary for the {@link javafx.fxml.FXMLLoader FXMLLoader}.
	 * @param list the List
	 * @return the Class of the Elements of the List
	 */
	public static Class<?> getEnumClass(final ObservableList<?> list) {
		return list.isEmpty() ? null : list.get(0).getClass();
	}

	/**
	 * Populates a {@link javafx.collections.ObservableList ObservableList} with
	 * all Constants of an {@link java.lang.Enum Enums}.
	 * @param list the populated List
	 * @param enumClass the Enum
	 * @see java.lang.Class#getEnumConstants()
	 */
	public static <T extends Enum<T>> void setEnumClass(
			final ObservableList<? super T> list,
			final Class<T> enumClass) {
		if (!enumClass.isEnum()) {
			throw new IllegalArgumentException(
					enumClass.getName() + " is not a enum type");
		}
		list.addAll(enumClass.getEnumConstants());
	}
}
