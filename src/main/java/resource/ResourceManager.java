package resource;

import static resource.ResourceType.CSS;
import static resource.ResourceType.FXML;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Matcher;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Dialog;
import javafx.util.Pair;

/**
 * Static Class to load JavaFX Resources like FXML and CSS.
 * @author Mario Schäper
 */
public abstract class ResourceManager {
	private static String resourcePath = "src.main.resources";
	private static final HashMap<String, Pair<Parent, Object>> resources =
			new HashMap<>();

	/**
	 * Returns the absolute Path of the Project Origin.
	 * @return
	 */
	public static String projectPath() {
		final String separator = Matcher.quoteReplacement(File.separator);
		final URL root = ClassLoader.getSystemClassLoader().getResource(".");
		if (root == null) {
			return File.separator
					+ System.getProperty("user.dir")
					+ File.separator
					+ ResourceManager.resourcePath.replace(".", File.separator);
		}
		final String path = root.getPath().replaceAll("[\\\\/]", separator);
		final String sequence = separator + "(\\w+)" + separator + "$";
		return path.replaceAll(
				sequence,
				separator
					+ ResourceManager.resourcePath.replace(".", separator));
	}

	/**
	 * Defines the Path, under which resources are stored,
	 * relative to the Project Origin.
	 * Folders in the Path should be separated by Dots.
	 * The Folder Structure has to look like the following:
	 * <pre>
	 * resources
	 *   fxml
	 *     package
	 *       Class.fxml
	 *     otherPackage
	 *       OtherClass.fxml
	 *   css
	 *     package
	 *       Class.css
	 *     otherPackage
	 *       OtherClass.fxml
	 * </pre>
	 * @param folders the Folders leading up to the Resources separated by Dots
	 */
	public static void setResourcePath(String...folders) {
		ResourceManager.resourcePath = String.join(".", folders);
	}

	/**
	 * Creates a {@link java.net.URL URL} of the given Elements Resource-Type.
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * @param resource the Type of the Resource
	 * @param element the Resource
	 * @return The resulting URL or <em>null</em> if it is malformed
	 */
	public static URL loadURL(ResourceType resource, String element) {
		URL url = null;
		try {
			url = new URL(
					String.join(
							File.separator,
							"file://",
							ResourceManager.projectPath(),
							resource.getFolder(),
							element.replace('.', File.separatorChar))
					+ "." + resource.getExtension());
		} catch (MalformedURLException e) {}
		return url;
	}

	/**
	 * Returns the FXML Resource with CSS applied.
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * <br/>Loaded Resources and their Controllers are cached.
	 * @param element the Resource
	 * @return the resulting {@link javafx.scene.layout.Pane Pane}
	 * 	/{@link javafx.scene.Scene Scene} as {@link javafx.scene.Parent Parent}
	 * @throws IOException if an IOError occurs while loading the Resources
	 */
	public static Parent getResource(String element) throws IOException {
		if (!ResourceManager.resources.containsKey(element)) {
			ResourceManager.chacheResource(element);
		}
		return ResourceManager.resources.get(element).getKey();
	}

	/**
	 * Returns the Controller associated by the Resource of the given Element.
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * <br/>Loaded Resources and their Controllers are cached.
	 * @param element the Element
	 * @param targetClass the {@link java.lang.Class Class} of the Controller
	 * @return the Controller associated by the Resource of the given Element
	 * @throws IOException if an IOError occurs while loading the Resources
	 * @throws ClassCastException if the Controller does not fit the targetClass
	 */
	public static <T> T getController(String element, Class<T> targetClass)
			throws IOException, ClassCastException {
		if (!ResourceManager.resources.containsKey(element)) {
			ResourceManager.chacheResource(element);
		}
		Object controller = ResourceManager.resources.get(element).getValue();
		return targetClass.cast(controller);
	}

	/**
	 * Initializes the given Resource by loading FXML and CSS from
	 * the Resource-Path and using the Parent as Controller.<br/>
	 * FXML and CSS are determined by the Element.
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * @param resource the Resource to initialize
	 * @param element the Element
	 * @param root the Root (may be <em>null</em>)
	 * @return <em>true</em> if successful, <em>false</em> otherwise
	 */
	public static boolean initResource(
			final Parent resource,
			final String element,
			final Object root) {
		final FXMLLoader loader = new FXMLLoader(
				ResourceManager.loadURL(ResourceType.FXML, element));
		loader.setController(resource);
		if (root != null) {
			loader.setRoot(root);
		}
		try {
			loader.load();
			URL sheet = ResourceManager.loadURL(ResourceType.CSS, element);
			resource.getStylesheets().add(sheet.toExternalForm());
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Initializes the given Resource by loading FXML and CSS from
	 * the Resource-Path and using the Parent as Controller.<br/>
	 * FXML and CSS are determined by the Element, which is assumed to be
	 * the Path of the Packages and the Name of the Class.
	 * @param resource the Resource to initialize
	 * @param root the Root (may be <em>null</em>)
	 * @return <em>true</em> if successful, <em>false</em> otherwise
	 */
	public static boolean initResource(
			final Parent resource,
			final Object root) {
		return ResourceManager.initResource(
				resource,
				resource.getClass().getName(), root);
	}

	/**
	 * Initializes the given Resource by loading FXML and CSS from
	 * the Resource-Path and using the Parent as Controller.<br/>
	 * FXML and CSS are determined by the Element, which is assumed to be
	 * the Path of the Packages and the Name of the Class.<br/>
	 * As a default the Parent will be used as Root.
	 * @param resource the Resource to initialize
	 * @return <em>true</em> if successful, <em>false</em> otherwise
	 */
	public static boolean initResource(final Parent resource) {
		return ResourceManager.initResource(resource, resource);
	}

	/**
	 * Creates a {@link javafx.scene.control.Dialog Dialog} from the given
	 * {@link java.lang.Class Class} and initializes it with the FXML and CSS
	 * associated by the given Element.<br/>
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * @param dialogClass the Class of the Dialog
	 * @param element the Element
	 * @return the Dialog
	 * @throws IOException if an IOError occurs
	 * @throws InstantiationException  if the Class or its nullary Constructor
	 * is not accessible
	 * @throws IllegalAccessException if the Class is not initializable
	 * or does not have a nullary Constructor
	 */
	public static <T extends Dialog<?>> T createDialog(
			final Class<T> dialogClass,
			final String element)
				throws IOException,
					InstantiationException,
					IllegalAccessException {
		try {
			final T dialog = dialogClass.getDeclaredConstructor().newInstance();
			ResourceManager.initDialog(dialog, element);
			return dialog;
		} catch (IllegalArgumentException
				| InvocationTargetException
				| SecurityException
				| NoSuchMethodException exception) {
			throw new InstantiationException(
					"Constructor without Arguments not invokable");
		}
	}

	/**
	 * Creates a {@link javafx.scene.control.Dialog Dialog} from the given
	 * {@link java.lang.Class Class} and initializes it with the FXML and CSS
	 * associated by the given Element, which is assumed to be
	 * the Path of the Packages and the Name of the Class.
	 * @param dialogClass the Class of the Dialog
	 * @return the Dialog
	 * @throws IOException if an IOError occurs
	 * @throws InstantiationException  if the Class or its nullary Constructor
	 * is not accessible
	 * @throws IllegalAccessException if the Class is not initializable
	 * or does not have a nullary Constructor
	 */
	public static <T extends Dialog<?>> T createDialog(
			final Class<T> dialogClass)
				throws IOException,
					InstantiationException,
					IllegalAccessException {
		return ResourceManager.createDialog(dialogClass, dialogClass.getName());
	}

	/**
	 * Initializes a {@link javafx.scene.control.Dialog Dialog} from the given
	 * with the FXML and CSS associated by the given Element.<br/>
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * @param dialog the Dialog
	 * @throws IOException if an IOError occurs
	 */
	public static void initDialog(
			final Dialog<?> dialog,
			final String element) throws IOException {
		dialog.getDialogPane().setContent(
				ResourceManager.loadNewResource(element, dialog));
	}

	/**
	 * Initializes a {@link javafx.scene.control.Dialog Dialog} from the given
	 * with the FXML and CSS associated by the given Element, which is assumed
	 * to be the Path of the Packages and the Name of the Class.
	 * @param dialog the Dialog
	 * @throws IOException if an IOError occurs
	 */
	public static void initDialog(final Dialog<?> dialog) throws IOException {
		ResourceManager.initDialog(dialog, dialog.getClass().getName());
	}

	/**
	 * Returns the FXML Resource with CSS applied and a specific Controller.
	 * Cascading Folders have to be indicated by Dots (e.g. "application.Main").
	 * @param element the Resource
	 * @param controller the Controller (my be <em>null</em>)
	 * @return the resulting {@link javafx.scene.layout.Pane Pane}
	 * 	/{@link javafx.scene.Scene Scene} as {@link javafx.scene.Parent Parent}
	 * @throws IOException if an IOError occurs while loading the Resources
	 */
	public static Parent loadNewResource(
			final String element,
			final Object controller) throws IOException {
		final FXMLLoader loader = new FXMLLoader(
				ResourceManager.loadURL(FXML, element));
		if (controller != null) {
			loader.setController(controller);
		}
		final Parent resource = loader.load();
		resource.getStylesheets().add(
				ResourceManager.loadURL(CSS, element).toExternalForm());
		return resource;
	}

	private static void chacheResource(final String element)
			throws IOException {
		final FXMLLoader loader = new FXMLLoader(
				ResourceManager.loadURL(FXML, element));
		final Parent resource = loader.load();
		resource.getStylesheets().add(
				ResourceManager.loadURL(CSS, element).toExternalForm());
		ResourceManager.resources.put(element, new Pair<>(
				resource, loader.getController()));
	}
}
