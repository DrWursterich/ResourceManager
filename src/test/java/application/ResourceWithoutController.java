package application;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class ResourceWithoutController extends HBox {
	@FXML
	private Label label;

	public ResourceWithoutController() {
		super();
	}

	public Label getLabel() {
		return this.label;
	}
}
