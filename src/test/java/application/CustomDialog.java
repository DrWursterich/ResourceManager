package application;

import javafx.fxml.FXML;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;

public class CustomDialog extends Dialog<String> {
	@FXML
	private Label label;

	public CustomDialog() {
		super();
	}

	public Label getLabel() {
		return this.label;
	}
}
