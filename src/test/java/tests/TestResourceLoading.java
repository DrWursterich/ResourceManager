package tests;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javax.swing.SwingUtilities;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import application.CustomDialog;
import application.ResourceWithoutController;
import controller.MainController;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import resource.ResourceManager;

/**
 * @author Mario Schäper
 */
public class TestResourceLoading {
	private boolean jfxIsSetup;

	private void doOnJavaFXThread(Runnable pRun) throws Exception {
		if (!this.jfxIsSetup) {
			this.setupJavaFX();
			this.jfxIsSetup = true;
		}
		final CountDownLatch countDownLatch = new CountDownLatch(1);
		Platform.runLater(() -> {
			pRun.run();
			countDownLatch.countDown();
		});
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	protected void setupJavaFX() throws RuntimeException {
		final CountDownLatch latch = new CountDownLatch(1);
		SwingUtilities.invokeLater(() -> {
			new JFXPanel();
			latch.countDown();
		});
		try {
			latch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@BeforeClass
	public static void setUp() {
		ResourceManager.setResourcePath("src.test.resources");
	}

	/**
	 * Test method for {@link resource.ResourceManager#getResource(String)}.
	 */
	@Test
	public final void testGetResource() throws Exception {
		final Parent resource = ResourceManager.getResource("application.Main");
		Assert.assertTrue(resource instanceof VBox);
		final List<Node> children = resource.getChildrenUnmodifiable();
		Assert.assertEquals(2, children.size());
		Assert.assertTrue(children.get(0) instanceof HBox);
		Assert.assertTrue(children.get(1) instanceof GridPane);
		final List<String> sheets = resource.getStylesheets();
		Assert.assertEquals(1, sheets.size());
		Assert.assertTrue(sheets.get(0).contains("Main.css"));
	}

	/**
	 * Test method for {@link resource.ResourceManager#getController(String)}.
	 */
	@Test
	public final void testGetController() throws Exception {
		final MainController controller = ResourceManager.getController("application.Main",
				MainController.class);
		Assert.assertEquals("Test succeeded", controller.getTestResult());
		final VBox vbox = controller.getVBox();
		Assert.assertNotNull(vbox);
		Assert.assertEquals(2, vbox.getChildren().size());
	}

	/**
	 * Test method for
	 * {@link resource.ResourceManager#initResource(Parent, String, boolean)}
	 */
	@Test
	public final void testInitResource() throws Exception {
		this.doOnJavaFXThread(() -> {
			final ResourceWithoutController resource =
					new ResourceWithoutController();
			Assert.assertTrue(ResourceManager.initResource(resource));
			Assert.assertNotNull(resource.getLabel());
			Assert.assertEquals("success", resource.getLabel().getText());
		});
	}

	/**
	 * Test method for
	 * {@link resource.ResourceManager
	 * #initDialog(javafx.scene.control.Dialog, String)}
	 */
	@Test
	public final void testInitDialog() throws Exception {
		this.doOnJavaFXThread(() -> {
			final CustomDialog dialog = new CustomDialog();
			try {
				ResourceManager.initDialog(dialog);
			} catch (IOException e) {
				e.printStackTrace();
				Assert.fail();
			}
			Assert.assertNotNull(dialog.getLabel());
			Assert.assertEquals("success", dialog.getLabel().getText());
		});
	}
}
