package controller;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class MainController {
	@FXML
	public VBox vbox;
	@FXML
	public HBox hbox;
	@FXML
	public GridPane gridpane;

	public MainController() {}

	public String getTestResult() {
		return "Test succeeded";
	}

	public VBox getVBox() {
		return this.vbox;
	}
}
